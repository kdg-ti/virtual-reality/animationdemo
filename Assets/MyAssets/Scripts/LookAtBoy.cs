﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtBoy : MonoBehaviour
{
    private Animator animator;

    void Start() {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        if (Google.XR.Cardboard.Api.IsTriggerPressed || Input.GetKey(KeyCode.Space)) {
            animator.SetBool("startWalking", true);
        }
    }
}
